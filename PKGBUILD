pkgname=lunaos-calamares
pkgver=3.3.14
pkgrel=2
pkgdesc='Distribution-independent installer framework'
arch=('x86_64')
license=('GPL3')
url="https://gitlab.com/LunaOS/iso/lunaos-calamares"
conflicts=('calamares')
provides=('calamares')
makedepends=('git' 'cmake' 'extra-cmake-modules' 'boost' 'python-jsonschema' 'python-pyaml' 'python-unidecode' 'ninja' 'qt6-tools')
depends=('qt6-svg' 'yaml-cpp' 'networkmanager' 'upower' 'kcoreaddons' 'kconfig' 'ki18n' 'kservice' 'kcrash'
         'kwidgetsaddons' 'kpmcore' 'squashfs-tools' 'rsync' 'cryptsetup' 'doxygen' 'dmidecode' 'qt6-declarative'
         'gptfdisk' 'hwinfo' 'kparts' 'polkit-qt6' 'python' 'solid' 'boost-libs' 'libpwquality' 'ckbcomp' 'mkinitcpio-openswap' 'wget' 'yad' 'inxi' "os-prober" 'kpmcore')
_commit=babaddffc39fa1a423c4f9714e8cf972f18d2af6
source=("calamares::git+$url#commit=$_commit"
        "calamares.desktop"
        "calamares_polkit")

sha256sums=('f9f0b04edd26efe8ccce195d276368851cd601d443ad9c76bcb0aff5bf2ba90a'
            '4f346bef29972f93d78f3370955e8036f99f25e51a27372b6df00c12b8db2433'
            'b1fa07cf458bdad4b933818c0821784cce417a354bf40f2147e867f7a89b7306')

options=('!strip' '!emptydirs')

prepare() {
    cd ${srcdir}/calamares

    # change version
    _ver="$(cat CMakeLists.txt | grep -m3 -e "  VERSION" | grep -o "[[:digit:]]*" | xargs | sed s'/ /./g')"
    _ver="$pkgver"
    printf 'Version: %s-%s\n' "${_ver}" "${pkgrel}"
    sed -i -e "s|\${CALAMARES_VERSION_MAJOR}.\${CALAMARES_VERSION_MINOR}.\${CALAMARES_VERSION_PATCH}|${_ver}-${pkgrel}|g" CMakeLists.txt
    sed -i -e "s|CALAMARES_VERSION_RC 1|CALAMARES_VERSION_RC 0|g" CMakeLists.txt
}

build() {
    cd ${srcdir}/calamares

    _cpuCount=$(grep -c -w ^processor /proc/cpuinfo)

    export CXXFLAGS+=" -fPIC"

    cmake -S . -Bbuild \
        -GNinja \
        -DCMAKE_BUILD_TYPE=Release \
        -DCMAKE_INSTALL_PREFIX=/usr \
        -DCMAKE_INSTALL_LIBDIR=lib \
        -DWITH_APPSTREAM=OFF \
        -DWITH_PYBIND11=OFF \
        -DWITH_QT6=ON \
        -DSKIP_MODULES="dracut \
        zfs zfshostid \
        initcpiocfg initramfs initramfscfg mkinitfs \
        luksopenswaphookcfg \
        dummycpp dummyprocess dummypython dummypythonqt \
        finishedq keyboardq license localeq notesqml oemid \
        openrcdmcryptcfg packagechooserq pacstrap plymouthcfg \
        plasmalnf services-openrc \
        summaryq tracking usersq webview welcomeq"

    cmake --build build --parallel $_cpuCount
}

package() {
    cd ${srcdir}/calamares/build
    DESTDIR="${pkgdir}" cmake --build . --target install

    local _destdir=etc/calamares
    install -dm755 $pkgdir/$_destdir
	install -Dm644 "${srcdir}/calamares.desktop" "$pkgdir/usr/share/applications/calamares.desktop"
	install -Dm755 "${srcdir}/calamares_polkit" "$pkgdir/usr/bin/calamares_polkit"
}
